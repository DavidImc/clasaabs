package abstractization;

public class AbstractizationMain {
    public static void main(String[] args) {

        Triangle myTriangle=new Triangle(4);
        System.out.println("Area of: myTriangle is: "+myTriangle.calculateArea());
        System.out.println("Colour of: myTriangle is: "+myTriangle.getColour()+" and is solid "+myTriangle.issSolid);


        Shape myShape=new Triangle(3);
        System.out.println("Area of: myTriangle is: "+myTriangle.calculateArea());
        System.out.println("Colour of: myTriangle is: "+myTriangle.getColour()+" and is solid "+ myShape.issSolid);

    }
}
