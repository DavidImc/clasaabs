package abstractization;

public abstract class Shape {
   public boolean issSolid=false;
    double area;
    int marginNumbers;
    int marginSize;

    public Shape(int marginNumbers, int marginSize) {
        this.marginNumbers = marginNumbers;
        this.marginSize = marginSize;
    }

   // public abstract double calculateArea(int marginNumbers, int marginSize);

    public abstract double calculateArea();

    public String getColou(){
        return "Red";
    }

}
