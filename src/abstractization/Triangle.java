package abstractization;

public class Triangle extends Shape{
    public boolean issSolid=true;

    public Triangle(int size){
        super(3,size);
    }


    public double calculateArea(int marginNumbers, int marginSize) {
        return marginNumbers*marginSize;
    }

    @Override
    public double calculateArea(){
        return marginNumbers*marginSize;
    }

    public String getColour(){
        return "Blue";
    }
}
