package InheritanceProject;

public class Main {


    public static void main(String[] args) {
        Cat cat =new Cat("Tom", "Blue russian",2,4,true);
        Cat cat3 =new Cat("Thomas", "Corci",4,4,true);

        //BAD
       // Cat cat2 =(Cat)new Animal("Tom", "Blue russian",2,4);

        //Bad
        //Cat cat3=(Cat) cat;

        cat.makeSound();
        displayCat(cat);
        ////polimorfism

      //  cat2.makeSound();
      //  displayCat(cat2);

        Cat cat1=new Cat("Bobi","British",3,4,false);

        Animal animal=cat1;
        Animal cat6=new Cat("Thom","Siamaze",6,3,true);
        animal.makeSound();
        displayCat(cat1);
        cat1.speakFromAnimal();


        Cat cat2 =new Cat("Ala", "Persana",2,4,true);

    }
    static void displayCat(Cat cat){
        System.out.println("Nume pisica: "+cat.getName()+ " are rasa: "+cat.getRace() );
    }
}
