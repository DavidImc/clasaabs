package InheritanceProject;

public class Cat extends Animal{
    boolean purr;
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Cat(String name, String race, int age, int legsNumber, boolean purr) {
        super(name,race,age, legsNumber);
        this.purr = purr;
        this.name="Nume Default";
    }

    public boolean isPurr() {
        return purr;
    }

    public void setPurr(boolean purr) {
        this.purr = purr;
    }

     public void makeSound(){
        System.out.println("miau miau");
    }

    public void speakFromAnimal(){
        super.makeSound();
    }

}


