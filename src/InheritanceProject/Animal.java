package InheritanceProject;

public  class Animal {
    private String name;
    private String race;
    private int varsta;
    private int legsNumber;

    public Animal(String name, String race, int varsta, int legsNumber) {
        this.name = name;
        this.race = race;
        this.varsta = varsta;
        this.legsNumber = legsNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public void setLegsNumber(int legsNumber) {
        this.legsNumber = legsNumber;
    }

    public String getName() {
        return name;
    }

    public String getRace() {
        return race;
    }

    public int getVarsta() {
        return varsta;
    }

    public int getLegsNumber() {
        return legsNumber;
    }

    protected void makeSound(){
        System.out.println("crr");
    }
}


